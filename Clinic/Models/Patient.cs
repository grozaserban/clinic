﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Clinic.Models
{
    public class Patient
    {
        public int ID { get; set; }
        [Required()]
        public string Surname { get; set; }
        [Required()]
        public string Name { get; set; }
        [RegularExpression(@"^[0-9]{1,40}$")]
        public string ICN { get; set; }
        [StringLength(13)]
        [RegularExpression(@"^[0-9]{1,40}$")]
        public string PNC { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime BirthDate { get; set; }
        public string Adress { get; set; }
     
    }
}