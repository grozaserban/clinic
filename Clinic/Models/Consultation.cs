﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Clinic.Models
{
    public class Consultation
    {
        public int ID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime Date { get; set; }
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true)]
        public DateTime Hour { get; set; }
        public string Details { get; set; }
        public Patient Patient { get; set; }
        public int PatientID { get; set; }
        public ApplicationUser Doctor { get; set; }
        public string DoctorID { get; set; }
    }
   
}