namespace Clinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datetimeconsultationformat4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Consultations", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Consultations", "Hour", c => c.DateTime(nullable: false));
            DropColumn("dbo.Consultations", "Time");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Consultations", "Time", c => c.DateTime(nullable: false));
            DropColumn("dbo.Consultations", "Hour");
            DropColumn("dbo.Consultations", "Date");
        }
    }
}
