// <auto-generated />
namespace Clinic.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class datetimeconsultationformat1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(datetimeconsultationformat1));
        
        string IMigrationMetadata.Id
        {
            get { return "201505161112528_datetimeconsultationformat1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
