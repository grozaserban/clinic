namespace Clinic.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedoctorid : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Consultations", new[] { "Doctor_Id" });
            DropColumn("dbo.Consultations", "DoctorID");
            RenameColumn(table: "dbo.Consultations", name: "Doctor_Id", newName: "DoctorID");
            AlterColumn("dbo.Consultations", "DoctorID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Consultations", "DoctorID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Consultations", new[] { "DoctorID" });
            AlterColumn("dbo.Consultations", "DoctorID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Consultations", name: "DoctorID", newName: "Doctor_Id");
            AddColumn("dbo.Consultations", "DoctorID", c => c.Int(nullable: false));
            CreateIndex("dbo.Consultations", "Doctor_Id");
        }
    }
}
